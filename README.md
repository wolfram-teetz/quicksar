# Quicksar is free open source tool that provides advanced visualization and analytics tools for chemical information.

This is a scaffold application to demonstate touch-enabled access to molecular information

Used Frameworks/Technolgies

Groovy/Grails - Code
Twitter Bootstrap - CSS
CDK - Molecule Handling
D3 - Visualisation - Heatmap
ChemDoodle WC - Molecule I/O, Painting