package quicksar



import org.junit.*
import grails.test.mixin.*

@TestFor(MoleculeSetController)
@Mock(MoleculeSet)
class MoleculeSetControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/moleculeSet/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.moleculeSetInstanceList.size() == 0
        assert model.moleculeSetInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.moleculeSetInstance != null
    }

    void testSave() {
        controller.save()

        assert model.moleculeSetInstance != null
        assert view == '/moleculeSet/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/moleculeSet/show/1'
        assert controller.flash.message != null
        assert MoleculeSet.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/moleculeSet/list'

        populateValidParams(params)
        def moleculeSet = new MoleculeSet(params)

        assert moleculeSet.save() != null

        params.id = moleculeSet.id

        def model = controller.show()

        assert model.moleculeSetInstance == moleculeSet
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/moleculeSet/list'

        populateValidParams(params)
        def moleculeSet = new MoleculeSet(params)

        assert moleculeSet.save() != null

        params.id = moleculeSet.id

        def model = controller.edit()

        assert model.moleculeSetInstance == moleculeSet
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/moleculeSet/list'

        response.reset()

        populateValidParams(params)
        def moleculeSet = new MoleculeSet(params)

        assert moleculeSet.save() != null

        // test invalid parameters in update
        params.id = moleculeSet.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/moleculeSet/edit"
        assert model.moleculeSetInstance != null

        moleculeSet.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/moleculeSet/show/$moleculeSet.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        moleculeSet.clearErrors()

        populateValidParams(params)
        params.id = moleculeSet.id
        params.version = -1
        controller.update()

        assert view == "/moleculeSet/edit"
        assert model.moleculeSetInstance != null
        assert model.moleculeSetInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/moleculeSet/list'

        response.reset()

        populateValidParams(params)
        def moleculeSet = new MoleculeSet(params)

        assert moleculeSet.save() != null
        assert MoleculeSet.count() == 1

        params.id = moleculeSet.id

        controller.delete()

        assert MoleculeSet.count() == 0
        assert MoleculeSet.get(moleculeSet.id) == null
        assert response.redirectedUrl == '/moleculeSet/list'
    }
}
