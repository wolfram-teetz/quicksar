package quicksar



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(MoleculeService)
class MoleculeServiceTests {

    void testSomething() {
        fail "Implement me"
    }
}
