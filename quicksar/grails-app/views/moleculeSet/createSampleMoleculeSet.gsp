<%@ page import="quicksar.MoleculeSet"%>
<!doctype html>
<html>
<head>
<meta name="layout" content="bootstrap">
<g:set var="entityName"
	value="${message(code: 'moleculeSet.label', default: 'MoleculeSet')}" />
<title><g:message code="default.create.label"
		args="[entityName]" /></title>
</head>
<body>
	<div class="row-fluid">

		<div class="page-header">
			<h1>Creating sample molecule set</h1>
		</div>
		<pre> A sample molecule set was created successfully.</pre> 
		You can find it <a href="/quicksar/moleculeSet/list">here.</a> and continue by calculating correlations from there.<br/>
		A sample descriptor set for it has also been calculated that can be found <a href="/quicksar/descriptorSet/list">here.</a><br/>
		
	</div>
</body>
</html>
