
<%@ page import="quicksar.MoleculeSet" %>
<!doctype html>
<html>
	<head>


      <!-- Chemdoodle -->
      <meta http-equiv="X-UA-Compatible" content="chrome=1">
	  <link rel="stylesheet" href="css/ChemDoodleWeb.css" type="text/css">
	  <script type="text/javascript" src="/quicksar/static/js/ChemDoodleWeb-libs.js"></script>
	  <script type="text/javascript" src="/quicksar/static/js/ChemDoodleWeb.js"></script>
      <!-- Chemdoodle ends -->
	  <script type="text/javascript" src="/quicksar/static/js/d3.v3.js"></script>


		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'moleculeSet.label', default: 'MoleculeSet')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span3">
				<div class="well">
					<ul class="nav nav-list">
						<li class="nav-header">${entityName}</li>
						<li>
							<g:link class="list" action="list">
								<i class="icon-list"></i>
								<g:message code="default.list.label" args="[entityName]" />
							</g:link>
						</li>
						<li>
							<g:link class="create" action="create">
								<i class="icon-plus"></i>
								<g:message code="default.create.label" args="[entityName]" />
							</g:link>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="span9">

				<div class="page-header">
					<h1><g:message code="default.show.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>


<%--TEST--%>

<h2>Heatmap</h2>
<h3>Descriptor Set : One</h3>

<g:select id="active" name="active" from="${['Enabled','Disabled']}" keys="${[1,0]}" value="${userInstance?.active}" />

<%--${mmds}--%>

	<div id='heatmap'></div>

	<script>
		${mol_js} // define data

		
		// Column molecule

		var colMol = new ChemDoodle.TransformCanvas('column', 400, 400, false); // 3d
		colMol.specs.bonds_useJMOLColors = true	
		colMol.specs.bonds_width_2D = 3;
		colMol.specs.atoms_display = true;
		colMol.specs.backgroundColor = 'white';
		colMol.specs.bonds_clearOverlaps_2D = true;
			var exampleMolFile = row2mol[0].mol;
			var mol = ChemDoodle.readMOL(exampleMolFile);
		colMol.loadMolecule(mol);		

		// Row molecule

		var rowMol = new ChemDoodle.TransformCanvas('row', 400, 400, false); // 3d
		rowMol.specs.bonds_useJMOLColors = true	
		rowMol.specs.bonds_width_2D = 3;
		rowMol.specs.atoms_display = true;
		rowMol.specs.backgroundColor = 'white';
		rowMol.specs.bonds_clearOverlaps_2D = true;
			var exampleMolFile = row2mol[0].mol;
			var mol = ChemDoodle.readMOL(exampleMolFile);
		rowMol.loadMolecule(mol);			
		
		// HEATMAP
		//height of each row in the heatmap
		//width of each column in the heatmap
		${heatmap_js} // define data

		var gridSize = 20, h = gridSize, w = gridSize, rectPadding = 60;

		var colorLow = 'green', colorMed = 'yellow', colorHigh = 'red';

		var margin = {
			top : 20,
			right : 20,
			bottom : 20,
			left : 20
		}, width = 800 - margin.left - margin.right, height = 400 - margin.top
				- margin.bottom;

		var colorScale = d3.scale.linear().domain([ -1, 0, 1 ]).range(
				[ colorLow, colorMed, colorHigh ]);

		var svg = d3.select("#heatmap").append("svg").attr("width",
				width + margin.left + margin.right).attr("height",
				height + margin.top + margin.bottom).append("g").attr(
				"transform",
				"translate(" + margin.left + "," + margin.top + ")");

		var selectCell = function(cell) {
		    d3.select(cell).attr("stroke", "#f00").attr("stroke-width", 3);

		    cell.parentNode.parentNode.appendChild(cell.parentNode);
		    cell.parentNode.appendChild(cell);
			    
		};

		// On click : "lock selected" to explore compound,  on click again "unlock"
		
		var deselectCell = function(cell) {
		    d3.select(cell).attr("stroke", "#fff").attr("stroke-width", 1);
		};

		var onCellOver = function(cell, data) {
		    selectCell(cell);

		    // Paint molecules for selected point
			colMol.loadMolecule(ChemDoodle.readMOL(row2mol[data.row].mol));		
			rowMol.loadMolecule(ChemDoodle.readMOL(row2mol[data.col].mol));		
	 	};

		var onCellOut = function(cell, data) {
		    deselectCell(cell);

		    //transform = new ChemDoodle.TransformCanvas('transform', 400, 400, false);
		};
		
		var heatMap = svg.selectAll(".heatmap").data(data, function(d) {
			return d.col + ':' + d.row;
		}).enter().append("svg:rect").attr("x", function(d) {
			return d.row * w;
	    }).on("mouseover", function(d) {
	        onCellOver(this, d);
	    }).on("mouseout", function(d) {
	        onCellOut(this, d);
	    }).attr("y", function(d) {
			return d.col * h;
		}).attr("width", function(d) {
			return w;
		}).attr("height", function(d) {
			return h;
		}).style("fill", function(d) {
			return colorScale(d.score);
		});
	</script>
	
<%--	TEST--%>


				<dl>
				
					<g:if test="${moleculeSetInstance?.molecule}">
						<dt><g:message code="moleculeSet.molecule.label" default="Molecule" /></dt>
						
							<g:each in="${moleculeSetInstance.molecule}" var="m">
							<dd><g:link controller="molecule" action="show" id="${m.id}">${m?.encodeAsHTML()}</g:link></dd>
							</g:each>
						
					</g:if>
				
					<g:if test="${moleculeSetInstance?.descriptor}">
						<dt><g:message code="moleculeSet.descriptor.label" default="Descriptor" /></dt>
						
							<g:each in="${moleculeSetInstance.descriptor}" var="d">
							<dd><g:link controller="descriptor" action="show" id="${d.id}">${d?.encodeAsHTML()}</g:link></dd>
							</g:each>
						
					</g:if>
				
					<g:if test="${moleculeSetInstance?.name}">
						<dt><g:message code="moleculeSet.name.label" default="Name" /></dt>
						
							<dd><g:fieldValue bean="${moleculeSetInstance}" field="name"/></dd>
						
					</g:if>
				
				</dl>

				<g:form>
					<g:hiddenField name="id" value="${moleculeSetInstance?.id}" />
					<div class="form-actions">
						<g:link class="btn" action="edit" id="${moleculeSetInstance?.id}">
							<i class="icon-pencil"></i>
							<g:message code="default.button.edit.label" default="Edit" />
						</g:link>
						<button class="btn btn-danger" type="submit" name="_action_delete">
							<i class="icon-trash icon-white"></i>
							<g:message code="default.button.delete.label" default="Delete" />
						</button>
					</div>
				</g:form>

			</div>

		</div>
	</body>
</html>
