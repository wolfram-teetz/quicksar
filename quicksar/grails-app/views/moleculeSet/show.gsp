
<%@ page import="quicksar.MoleculeSet" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'moleculeSet.label', default: 'MoleculeSet')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span3">
				<div class="well">
					<ul class="nav nav-list">
						<li class="nav-header">${entityName}</li>
						<li>
							<g:link class="list" action="list">
								<i class="icon-list"></i>
								<g:message code="default.list.label" args="[entityName]" />
							</g:link>
						</li>
						<li>
							<g:link class="create" action="create">
								<i class="icon-plus"></i>
								<g:message code="default.create.label" args="[entityName]" />
							</g:link>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="span9">

				<div class="page-header">
					<h1><g:message code="default.show.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>

				<dl>
				
					<g:if test="${moleculeSetInstance?.molecule}">
						<dt><g:message code="moleculeSet.molecule.label" default="Molecule" /></dt>
						
							<g:each in="${moleculeSetInstance.molecule}" var="m">
							<dd><g:link controller="molecule" action="show" id="${m.id}">${m?.encodeAsHTML()}</g:link></dd>
							</g:each>
						
					</g:if>
				
					<g:if test="${moleculeSetInstance?.descriptorSet}">
						<dt><g:message code="moleculeSet.descriptorSet.label" default="DescriptorSet" /></dt>
						
							<g:each in="${moleculeSetInstance.descriptorSet}" var="d">
							<dd><g:link controller="descriptorSet" action="show" id="${d.id}">${d?.encodeAsHTML()}</g:link></dd>
							</g:each>
						
					</g:if>
				
					<g:if test="${moleculeSetInstance?.name}">
						<dt><g:message code="moleculeSet.name.label" default="Name" /></dt>
						
							<dd><g:fieldValue bean="${moleculeSetInstance}" field="name"/></dd>
						
					</g:if>
				
				</dl>

				<g:form>
					<g:hiddenField name="id" value="${moleculeSetInstance?.id}" />
					<div class="form-actions">
						<g:link class="btn" action="edit" id="${moleculeSetInstance?.id}">
							<i class="icon-pencil"></i>
							<g:message code="default.button.edit.label" default="Edit" />
						</g:link>
						<button class="btn btn-danger" type="submit" name="_action_delete">
							<i class="icon-trash icon-white"></i>
							<g:message code="default.button.delete.label" default="Delete" />
						</button>
					</div>
				</g:form>

			</div>

		</div>
	</body>
</html>
