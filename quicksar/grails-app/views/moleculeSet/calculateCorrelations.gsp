<%@ page import="quicksar.MoleculeSet"%>
<!doctype html>
<html>
<head>
<meta name="layout" content="bootstrap">
<g:set var="entityName"
	value="${message(code: 'moleculeSet.label', default: 'MoleculeSet')}" />
<title><g:message code="default.create.label"
		args="[entityName]" /></title>
</head>
<body>
	<div class="row-fluid">

		<div class="page-header">
			<h1>Calculating correlations</h1>
		</div>
		<pre> The intra-dataset correlations have been calculated. Result : ${result}.</pre> 
		You can now view the correlation heatmap <a href="/quicksar/moleculeSet/heatmap/${id}">here.</a><br/>
	
	</div>
</body>
</html>
