<%@ page import="quicksar.MoleculeSet" %>



<div class="fieldcontain ${hasErrors(bean: moleculeSetInstance, field: 'molecule', 'error')} ">
	<label for="molecule">
		<g:message code="moleculeSet.molecule.label" default="Molecule" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${moleculeSetInstance?.molecule?}" var="m">
    <li><g:link controller="molecule" action="show" id="${m.id}">${m?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="molecule" action="create" params="['moleculeSet.id': moleculeSetInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'molecule.label', default: 'Molecule')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: moleculeSetInstance, field: 'descriptor', 'error')} ">
	<label for="descriptor">
		<g:message code="moleculeSet.descriptor.label" default="Descriptor" />
		
	</label>
	<g:select name="descriptor" from="${quicksar.Descriptor.list()}" multiple="multiple" optionKey="id" size="5" value="${moleculeSetInstance?.descriptor*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: moleculeSetInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="moleculeSet.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${moleculeSetInstance?.name}"/>
</div>

