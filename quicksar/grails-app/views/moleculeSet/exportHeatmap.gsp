<html>
<head>
<meta name="layout" content="main" />
<r:require modules="bootstrap" />
</head>

<body>



<h1>Molecule set "One"</h1>
<h2>Dataset analysis</h2>
<h3>Descriptor Set : One</h3>
<h4>Heatmap</h4>

<%--${mmds}--%>

	<div id='heatmap'></div>

	<script>
		${mol_js} // define data

		
		// Column molecule

		var colMol = new ChemDoodle.TransformCanvas('column', 400, 400, false); // 3d
		colMol.specs.bonds_useJMOLColors = true	
		colMol.specs.bonds_width_2D = 3;
		colMol.specs.atoms_display = true;
		colMol.specs.backgroundColor = 'white';
		colMol.specs.bonds_clearOverlaps_2D = true;
			var exampleMolFile = row2mol[0].mol;
			var mol = ChemDoodle.readMOL(exampleMolFile);
		colMol.loadMolecule(mol);		

		// Row molecule

		var rowMol = new ChemDoodle.TransformCanvas('row', 400, 400, false); // 3d
		rowMol.specs.bonds_useJMOLColors = true	
		rowMol.specs.bonds_width_2D = 3;
		rowMol.specs.atoms_display = true;
		rowMol.specs.backgroundColor = 'white';
		rowMol.specs.bonds_clearOverlaps_2D = true;
			var exampleMolFile = row2mol[0].mol;
			var mol = ChemDoodle.readMOL(exampleMolFile);
		rowMol.loadMolecule(mol);			
		
		// HEATMAP
		//height of each row in the heatmap
		//width of each column in the heatmap
		${heatmap_js} // define data

		var gridSize = 20, h = gridSize, w = gridSize, rectPadding = 60;

		var colorLow = 'green', colorMed = 'yellow', colorHigh = 'red';

		var margin = {
			top : 20,
			right : 20,
			bottom : 20,
			left : 20
		}, width = 800 - margin.left - margin.right, height = 400 - margin.top
				- margin.bottom;

		var colorScale = d3.scale.linear().domain([ -1, 0, 1 ]).range(
				[ colorLow, colorMed, colorHigh ]);

		var svg = d3.select("#heatmap").append("svg").attr("width",
				width + margin.left + margin.right).attr("height",
				height + margin.top + margin.bottom).append("g").attr(
				"transform",
				"translate(" + margin.left + "," + margin.top + ")");

		var selectCell = function(cell) {
		    d3.select(cell).attr("stroke", "#f00").attr("stroke-width", 3);

		    cell.parentNode.parentNode.appendChild(cell.parentNode);
		    cell.parentNode.appendChild(cell);
			    
		};

		// On click : "lock selected" to explore compound,  on click again "unlock"
		
		var deselectCell = function(cell) {
		    d3.select(cell).attr("stroke", "#fff").attr("stroke-width", 1);
		};

		var onCellOver = function(cell, data) {
		    selectCell(cell);

		    // Paint molecules for selected point
			colMol.loadMolecule(ChemDoodle.readMOL(row2mol[data.row].mol));		
			rowMol.loadMolecule(ChemDoodle.readMOL(row2mol[data.col].mol));		
	 	};

		var onCellOut = function(cell, data) {
		    deselectCell(cell);

		    //transform = new ChemDoodle.TransformCanvas('transform', 400, 400, false);
		};
		
		var heatMap = svg.selectAll(".heatmap").data(data, function(d) {
			return d.col + ':' + d.row;
		}).enter().append("svg:rect").attr("x", function(d) {
			return d.row * w;
	    }).on("mouseover", function(d) {
	        onCellOver(this, d);
	    }).on("mouseout", function(d) {
	        onCellOut(this, d);
	    }).attr("y", function(d) {
			return d.col * h;
		}).attr("width", function(d) {
			return w;
		}).attr("height", function(d) {
			return h;
		}).style("fill", function(d) {
			return colorScale(d.score);
		});
	</script>

</body>
</html>
