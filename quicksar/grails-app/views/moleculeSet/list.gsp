
<%@ page import="quicksar.MoleculeSet"%>
<!doctype html>
<html>
<head>
<meta name="layout" content="bootstrap">
<g:set var="entityName"
	value="${message(code: 'moleculeSet.label', default: 'MoleculeSet')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="row-fluid">

		<div class="span3">
			<div class="well">
				<ul class="nav nav-list">
					<li class="nav-header">
						${entityName}
					</li>
					<li class="active"><g:link class="list" action="list">
							<i class="icon-list icon-white"></i>
							<g:message code="default.list.label" args="[entityName]" />
						</g:link></li>
					<li><g:link class="create" action="create">
							<i class="icon-plus"></i>
							<g:message code="default.create.label" args="[entityName]" />
						</g:link></li>
				</ul>
			</div>
		</div>

		<div class="span9">

			<div class="page-header">
				<h1>
					<g:message code="default.list.label" args="[entityName]" />
				</h1>
			</div>

			<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">
					${flash.message}
				</bootstrap:alert>
			</g:if>

			<table class="table table-striped">
				<thead>
					<tr>

						<g:sortableColumn property="name"
							title="${message(code: 'moleculeSet.name.label', default: 'Name')}" />

						<th></th>
					</tr>
				</thead>
				<tbody>

					<g:each in="${moleculeSetInstanceList}" var="moleculeSetInstance">
						<tr>

							<td>
								${fieldValue(bean: moleculeSetInstance, field: "name")}
							</td>

							<td class="link"><g:link action="show"
									id="${moleculeSetInstance.id}" class="btn btn-small">Show &raquo;</g:link>
							</td>

							<%--#CUSTOMIZED START--%>
							<td class="link"><g:link action="calculateCorrelations"
									id="${moleculeSetInstance.id}" class="btn btn-small">Calculate Correlations &raquo;</g:link>
							</td>

							<td class="link"><g:link action="heatmap"
									id="${moleculeSetInstance.id}" class="btn btn-small">Correlation Heatmap &raquo;</g:link>
							</td>
							<%--#CUSTOMIZED END--%>

						</tr>
					</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<bootstrap:paginate total="${moleculeSetInstanceTotal}" />
			</div>
		</div>

	</div>
</body>
</html>
