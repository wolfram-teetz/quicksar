<!DOCTYPE html>
<html lang="en">
   <head>
      <g:layoutTitle/>
      <r:layoutResources/>
      <!-- Chemdoodle -->
      <meta http-equiv="X-UA-Compatible" content="chrome=1">
	  <link rel="stylesheet" href="css/ChemDoodleWeb.css" type="text/css">
	  <script type="text/javascript" src="/quicksar/static/js/ChemDoodleWeb-libs.js"></script>
	  <script type="text/javascript" src="/quicksar/static/js/ChemDoodleWeb.js"></script>
      <!-- Chemdoodle ends -->
	  <script type="text/javascript" src="/quicksar/static/js/d3.v3.js"></script>
      
   </head>
   <body>
      <g:layoutBody/>
      <r:layoutResources/>
   </body>
</html>


