package quicksar

import org.openscience.cdk.DefaultChemObjectBuilder
import org.openscience.cdk.exception.CDKException
import org.openscience.cdk.interfaces.IAtomContainer
import org.openscience.cdk.io.MDLV2000Writer
import org.openscience.cdk.io.listener.PropertiesListener
import org.openscience.cdk.layout.StructureDiagramGenerator
import org.openscience.cdk.smiles.SmilesParser

class MoleculeService {

	DescriptorService descriptorService;

	Molecule saveMoleculeReplaceIfExists(String _smiles, MoleculeSet _moleculeSet)
	{

		println _moleculeSet
		// Save a
		def _molecule
		def _moleculeExists = Molecule.findBySmiles(_smiles)
		if (_moleculeExists == null) {
			_molecule = new Molecule(smiles : _smiles, inchiKey : "", moleculeSet : _moleculeSet)
		} else {
			_molecule = _moleculeExists
		}
		_molecule.save(flush:true, failOnError:true)
		return _molecule;
	}

	String MoleculeToMol(Molecule _molecule) // From a list of molecules, create a list of mol files with coordinates for painting
	{

		SmilesParser sp = new SmilesParser(DefaultChemObjectBuilder.getInstance());
		//IAtomContainer molecule = sp.parseSmiles("c1ccccc1");
		IAtomContainer molecule = sp.parseSmiles(_molecule.smiles);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		MDLV2000Writer writer = new MDLV2000Writer(baos);

		//force 2D coordinates even if 3D exists
		Properties customSettings = new Properties();
		customSettings.setProperty("ForceWriteAs2DCoordinates", "true");
		PropertiesListener listener = new PropertiesListener(customSettings);
		writer.addChemObjectIOListener(listener);
		//end force 2D coordinates

		//write the molecule to the outputstream
		try {
			//writer.write(null)
			StructureDiagramGenerator sdg = new StructureDiagramGenerator();
			sdg.setUseTemplates(true);
			sdg.setMolecule(molecule);
			sdg.generateCoordinates();
			writer.write(sdg.getMolecule());

			//			writer.write(molecule);
		} catch (CDKException e) {
			e.printStackTrace();
		}

		//convert the outputstream to a string
		String MoleculeString = baos.toString();

		//now split MoleculeString into multiple lines to enable explicit printout of \n
		String[] Moleculelines = MoleculeString.split("\\r?\\n");

		//now we print the javascript variable in the canvas
		StringWriter outfile = new StringWriter();
		//		outfile.print("var myMolFile = '");
		for(int x=0; x<Moleculelines.length; x++){
			outfile.print(Moleculelines[x]);
			outfile.print("\\n");
		}
		return outfile
	}
}
