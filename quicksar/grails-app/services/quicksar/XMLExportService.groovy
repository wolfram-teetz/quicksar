package quicksar

import groovy.xml.MarkupBuilder


class XMLExportService {

	// TODO, Nothing works, just a first scaffold
	
	def xmlFun() {
		def file = new File("test.xml")
		def objs = [
			[ quantity: 10, name: "Orange", type: "Fruit" ],
			[ quantity: 6, name: "Apple", type: "Fruit" ],
			[ quantity: 2, name: "Chair", type: "Furniture" ] ]
		def b = new MarkupBuilder(new FileWriter(file))
		b.root {
			objs.each { o ->
				item(qty: o.quantity) {
					name(o.name)
					type(o.type)
				}
			}
		}

		def xml = new XmlSlurper().parse(file)
		assert xml.item.size() == 3
		assert xml.item[0].name == "Orange"
		assert xml.item[0].@qty == "10"
		def returnString = ["Fruits: ${xml.item.findAll {it.type == 'Fruit'}*.name }",
			"Total: ${xml.item.@qty.list().sum {it.toInteger()} }" ]
	}

	//  OpenBabel conversion by native lib
	//	def molfile() {
	//		// Initialise
	//		System.loadLibrary("openbabel_java");
	//
	//		// Read molecule from SMILES string
	//		OBConversion conv = new OBConversion();
	//		OBMol mol = new OBMol();
	//		conv.SetInFormat("smi");
	//		conv.ReadString(mol, "C(Cl)(=O)CCC(=O)Cl");
	//
	//		// Print out some general information
	//		conv.SetOutFormat("mol");
	//		System.out.print("MOL: " +
	//		  conv.WriteString(mol));
	//	}
	
}
