package quicksar

import javax.print.attribute.standard.PrinterResolution;


class MoleculeSetService {

	DescriptorSetService descriptorSetService
	MoleculeService moleculeService
	MoleculeMoleculeDescriptorSetService moleculeMoleculeDescriptorSetService

	def createSampleMoleculeSet() {
		String _return = "";
		_return += createOneSampleMoleculeSet(1, 12, 8);
		_return += createOneSampleMoleculeSet(2, 5, 6); // tiny		
	}
	
	def createOneSampleMoleculeSet(int nr, int nmolecules, int moleculeLength) {

		def MoleculeSet _moleculeSet
		def moleculeSetExists = MoleculeSet.findByName("TestSet"+nr)
		if (moleculeSetExists == null) {
			_moleculeSet = new MoleculeSet(name : "TestSet"+nr, descriptorSet : null )
		} else {
			_moleculeSet = moleculeSetExists
		}
		_moleculeSet.save(flush:true, failOnError:true)  ;

		// If the Testset exists, remove all compounds from there
		def __mol = Molecule.findAllByMoleculeSet(_moleculeSet); // remove all molecules in this set existing
		for (int i=0; i<__mol.size(); i++) {
			def __mmds
			__mmds = MoleculeMoleculeDescriptorSet.findAllByMolecule1(__mol[i]); // remove all dependecies
			for (int j=0; j<__mmds.size(); j++) {
				__mmds[j].delete();
			}
			__mmds = MoleculeMoleculeDescriptorSet.findAllByMolecule2(__mol[i]);
			for (int j=0; j<__mmds.size(); j++) {
				__mmds[j].delete();
			}
			__mol[i].delete();
		}
		
		// Fill with random linear molecules of C,N and O atoms from SMILES
		for (int i=0; i<nmolecules; i++)
		{
			String ms="";

			for (int j=0; j<moleculeLength; j++) {
				int x = Math.random()*3.0
				switch (x)
				{
					case 0: ms += "C"; break;
					case 1: ms += "N"; break;
					case 2: ms += "O"; break;
				}
			}					

			moleculeService.saveMoleculeReplaceIfExists( ms, _moleculeSet) // TODO:: Call everything twice to create two datasets
			
			// Cut here and make 2 separe methods, create descriptor sets and calculate descriptor set for molecule set
			
			// Create 2 example descriptor sets
					
			descriptorSetService.calculateQNPRDescriptorSet(_moleculeSet, 4)
			descriptorSetService.calculateQNPRDescriptorSet(_moleculeSet, 5)
			
			// TODO Add descriptors to Datasets
			
			//println ms
		}
	}
	
	def calculateCorrelations(String _moleculeSetId) {

	//def molecules = Molecule.findAllByMoleculeSet(_moleculeSet).list();
		def moleculeSet = MoleculeSet.findById(_moleculeSetId); // MoleculeSet.findById(_moleculeSet.id);

		if (moleculeSet == null) return "failure : moleculeSet == null in calculateCorrelations"
		
		def molecules = Molecule.createCriteria().list {
			eq('moleculeSet',moleculeSet) 
		}

		Set descriptorSets = moleculeSet.getDescriptorSet()
		
		//def descriptorSets = DescriptorSet.findAllByMoleculeSet(moleculeSet);
		if (descriptorSets.size() < 1) return "failure : moleculeSet does not have any descriptorSets"
		//def descriptorSet = descriptorSets.first() // use the first best

		// TODO::CLEAN THE ALREADY CALCULATED VALUES
		
		
		// For each of the associated descriptor setc, calculate distance for all molecule-molecule pairs... ok with id1<=id2 is enough, symmetric..
		
		descriptorSets.each { // Groovy ;)
			
			def descriptorSet = it

			for (int i=0; i<molecules.size(); i++) {
				for (int j=0; j<molecules.size(); j++) {
					if (molecules[i].id>molecules[j].id) continue; // by definition of MoleculeMoleculeDescriptorSet
					//				descriptorSetService.compareMoleculesByDescriptorSet(molecules[i], molecules[j], descriptorSetService.getQNPRDescriptorSetAndCreateIfEmpty())
					descriptorSetService.compareMoleculesByDescriptorSet(molecules[i], molecules[j], descriptorSet)
				}
			}
			
			moleculeSet.addToCorrelationDescriptorSet(descriptorSet); // Save that this correlation has been calculated


			//		quicksar.Molecule molecule1 = saveMolecule( "CCOCCCOCC(=O)C", qnprDescriptorSet, _moleculeSet)
			//		quicksar.Molecule molecule2 = saveMolecule( "CCOCCCC", qnprDescriptorSet, _moleculeSet)
			//		return molecules[0];
/*
			def _return = MoleculeMoleculeDescriptorSet.createCriteria().list {
				createAlias("molecule1", "m1")
				createAlias("molecule2", "m2")
				and {
					eq('descriptorSet', descriptorSet)
					eq('m1.moleculeSet', moleculeSet)
					eq('m2.moleculeSet', moleculeSet)
				}
				order('m1.id','asc')
				order('m2.id','asc')
			}*/
			
		}

		//List data = moleculeMoleculeDescriptorSetService.MoleculeMoleculeDescriptorSetToD3Data(_return); // data for heatmap
		
		//return data;
		return "success"
		
	}

}
