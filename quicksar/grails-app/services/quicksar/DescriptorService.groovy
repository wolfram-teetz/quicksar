package quicksar

class DescriptorService {

	def calculateQNPRDescriptors(Molecule _molecule, DescriptorSet _descriptorSet,  int _length) // TODO:: Length should be stored in JSON parameter in descriptorSet
	{
		def _alredyHasIntegerDescriptors = IntegerDescriptor.findAllByMolecule(_molecule); // this molecule has already been analyzed
		// could return or refresh : do refresh
		for (int i=0; i<_alredyHasIntegerDescriptors.size(); i++ )
			_alredyHasIntegerDescriptors[i].delete(flush:true, failOnError:true)


		for (int i=_length; i<=_molecule.smiles.length(); i++)
		{
			def _name = _molecule.smiles.substring(i-_length, i)
			//			println _name

			def _descriptor

			def _descriptorExists = Descriptor.findByName(_name);
			if (_descriptorExists == null) {
				_descriptor = new Descriptor(descriptorSet : _descriptorSet, name : _name ); // should be increase
			} else {
				_descriptor = _descriptorExists
			}

			//println _descriptor.name
			//println _descriptor.descriptorSet
			_descriptor.save(flush:true, failOnError:true)
			//println "Descriptors : " + Descriptor.count


			def _integerDescriptor
			def _integerDescriptorExists = IntegerDescriptor.findByMoleculeAndDescriptor(_molecule, _descriptor);
			if (_integerDescriptorExists == null) {
				_integerDescriptor = new IntegerDescriptor(descriptor : _descriptor, molecule : _molecule, value : 1);
			} else {
				_integerDescriptor = _integerDescriptorExists
				_integerDescriptor.value++
			}
			_integerDescriptor.save(flush:true, failOnError:true)
		}
		return _molecule;

	}

}
