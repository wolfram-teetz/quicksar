package quicksar


class MoleculeMoleculeDescriptorSetService {

	def MoleculeService moleculeService;

	def MoleculeMoleculeDescriptorSetToD3Data(List _mmds ) // for d3 heatmap
	{

		List _list = []

		String _return = ""

		// data for painting molecules : lists of row/column to mol text for painting

		_return += "var row2mol = ["
		Map molecule1ToRow = [:]
		int molecule1ToRow_n = 0;
		for (int i=0; i<_mmds.size(); i++) {
			if (molecule1ToRow.get(_mmds[i].molecule1.id) == null) {
				molecule1ToRow.put(_mmds[i].molecule1.id, molecule1ToRow_n) // save in map for use in writing down the heatmap later
				_return += "{row : " + molecule1ToRow_n +", mol : \""+moleculeService.MoleculeToMol(_mmds[i].molecule1)+"\"},\n"
				molecule1ToRow_n++
			}
		}
		_return += "{row : -1, mol : \"\"}];\n"

		_return += "var col2mol = ["
		Map molecule2ToColumn = [:]
		int molecule2ToColumn_n = 0;
		for (int i=0; i<_mmds.size(); i++) {
			if (molecule2ToColumn.get(_mmds[i].molecule2.id) == null) {
				molecule2ToColumn.put(_mmds[i].molecule2.id, molecule2ToColumn_n) // save in map for use in writing down the heatmap later
				_return += "{col : " + molecule2ToColumn_n +", mol : \""+moleculeService.MoleculeToMol(_mmds[i].molecule2)+"\"},\n"
				molecule2ToColumn_n++
			}
		}
		_return += "{col : -1, mol : \"\"}];\n"

		_list += _return;
		_return = "";

		//	heatmap data

		_return+="var data = ["


		for (int i=0; i<_mmds.size(); i++) {
			int hit = _mmds[i].hit;
			int miss = _mmds[i].miss;
			_return += "{ row : "+molecule1ToRow.get(_mmds[i].molecule1.id)+","
			_return += "col : "+molecule2ToColumn.get(_mmds[i].molecule2.id)+","
			_return += "score : "+( hit/(hit+miss))+"}"
			if (i<_mmds.size()-1) _return += ",\n"
		}

		_return += "];\n"

		_list += _return;
		_return = "";

		return _list; // 2 Elements : row2mol&col2mol definition string, (heatmap) data definition string
	}

}
