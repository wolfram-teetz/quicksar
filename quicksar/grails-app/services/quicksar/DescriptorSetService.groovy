package quicksar

class DescriptorSetService {
	
	DescriptorService descriptorService

	DescriptorSet getQNPRDescriptorSetAndCreateIfEmpty(int _length)
	{
		def DescriptorSet qnprDescriptorSet
		def qnprDescriptorSetExists = DescriptorSet.findByName("QNPR"+_length);
		if (qnprDescriptorSetExists == null) {
			qnprDescriptorSet = new DescriptorSet(name : "QNPR"+_length);
		} else {
			qnprDescriptorSet = qnprDescriptorSetExists
		}
		qnprDescriptorSet.save(flush:true, failOnError:true)
		return qnprDescriptorSet
	}
	
	def calculateQNPRDescriptorSet(MoleculeSet _moleculeSet, int _length)
	{
		def _descriptorSet = getQNPRDescriptorSetAndCreateIfEmpty(_length)
		def _molecules = Molecule.findAllByMoleculeSet(_moleculeSet)

		for (int i=0; i<_molecules.size(); i++)
		{
			descriptorService.calculateQNPRDescriptors( _molecules[i], _descriptorSet, _length)			
		}
		_moleculeSet.addToDescriptorSet(_descriptorSet)
	}

	def compareMoleculesByDescriptorSet(Molecule molecule1, Molecule molecule2, DescriptorSet _descriptorSet)
	{
		def id1 = IntegerDescriptor.createCriteria().list {
			eq('molecule', molecule1)
			order("descriptor.id", "asc")
		}

		def id2 = IntegerDescriptor.createCriteria().list {
			eq('molecule', molecule2)
			order("descriptor.id", "asc")
		}

		int hit = 0;
		int miss = 0;

		int i=0;
		int j=0;
		fx : for (i=0; i<id1.size(); i++)
		{
			while (id1[i].descriptor.id > id2[j].descriptor.id) {
				j++; // break if j too large
				if (j>=id2.size()) break fx;
				miss++;
			}
			if (id1[i].descriptor.id == id2[j].descriptor.id) {
				hit++;
			}
			else {
				miss++;
			}
		}
		for (; j<id2.size(); j++)
		{
			miss++;
		}


		// ***** distances

		MoleculeMoleculeDescriptorSet mmdst = new MoleculeMoleculeDescriptorSet(
				molecule1 : molecule1,
				molecule2 : molecule2,
				descriptorSet : _descriptorSet,
				hit : hit,
				miss : miss
				) // miss/(hit/(hit+miss)) );

		mmdst.save(flush:true, failOnError:true)  ;

		return molecule1
	}

}
