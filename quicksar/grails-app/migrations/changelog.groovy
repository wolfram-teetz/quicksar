databaseChangeLog = {

	changeSet(author: "sir (generated)", id: "1378750415453-1") {
		createTable(tableName: "descriptor") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "descriptorPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "descriptor_set_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "sir (generated)", id: "1378750415453-2") {
		createTable(tableName: "descriptor_set") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "descriptor_sePK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "sir (generated)", id: "1378750415453-3") {
		createTable(tableName: "integer_descriptor") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "integer_descrPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "descriptor_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "molecule_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "value", type: "integer") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "sir (generated)", id: "1378750415453-4") {
		createTable(tableName: "molecule") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "moleculePK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "inchikey", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "mdlv", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "smiles", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "sir (generated)", id: "1378750415453-5") {
		createTable(tableName: "molecule_molecule_descriptor_set") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "molecule_molePK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "descriptor_set_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "disimilarity_quotient", type: "double") {
				constraints(nullable: "false")
			}

			column(name: "manhattan_distance", type: "integer") {
				constraints(nullable: "false")
			}

			column(name: "molecule1_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "molecule2_id", type: "bigint") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "sir (generated)", id: "1378750415453-6") {
		createTable(tableName: "quote") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "quotePK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "author", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "content", type: "varchar(1000)") {
				constraints(nullable: "false")
			}

			column(name: "created", type: "timestamp") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "sir (generated)", id: "1378750415453-7") {
		addForeignKeyConstraint(baseColumnNames: "descriptor_set_id", baseTableName: "descriptor", constraintName: "FKD364D36F57BA8E5B", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "descriptor_set", referencesUniqueColumn: "false")
	}

	changeSet(author: "sir (generated)", id: "1378750415453-8") {
		addForeignKeyConstraint(baseColumnNames: "descriptor_id", baseTableName: "integer_descriptor", constraintName: "FK6DC91510DB50E05A", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "descriptor", referencesUniqueColumn: "false")
	}

	changeSet(author: "sir (generated)", id: "1378750415453-9") {
		addForeignKeyConstraint(baseColumnNames: "molecule_id", baseTableName: "integer_descriptor", constraintName: "FK6DC915102FD034FA", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "molecule", referencesUniqueColumn: "false")
	}

	changeSet(author: "sir (generated)", id: "1378750415453-10") {
		addForeignKeyConstraint(baseColumnNames: "descriptor_set_id", baseTableName: "molecule_molecule_descriptor_set", constraintName: "FKEAC613B257BA8E5B", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "descriptor_set", referencesUniqueColumn: "false")
	}

	changeSet(author: "sir (generated)", id: "1378750415453-11") {
		addForeignKeyConstraint(baseColumnNames: "molecule1_id", baseTableName: "molecule_molecule_descriptor_set", constraintName: "FKEAC613B28E1992F5", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "molecule", referencesUniqueColumn: "false")
	}

	changeSet(author: "sir (generated)", id: "1378750415453-12") {
		addForeignKeyConstraint(baseColumnNames: "molecule2_id", baseTableName: "molecule_molecule_descriptor_set", constraintName: "FKEAC613B28E1A0754", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "molecule", referencesUniqueColumn: "false")
	}
}
