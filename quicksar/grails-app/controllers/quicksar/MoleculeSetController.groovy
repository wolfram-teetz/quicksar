package quicksar

import org.springframework.dao.DataIntegrityViolationException

class MoleculeSetController {

	def DescriptorSetService descriptorSetService
	def MoleculeMoleculeDescriptorSetService moleculeMoleculeDescriptorSetService
	def MoleculeSetService moleculeSetService

    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']
	
	XMLExportService quoteService;

    def index() {
        redirect action: 'list', params: params
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [moleculeSetInstanceList: MoleculeSet.list(params), moleculeSetInstanceTotal: MoleculeSet.count()]
    }

    def create() {
		switch (request.method) {
		case 'GET':
        	[moleculeSetInstance: new MoleculeSet(params)]
			break
		case 'POST':
	        def moleculeSetInstance = new MoleculeSet(params)
	        if (!moleculeSetInstance.save(flush: true)) {
	            render view: 'create', model: [moleculeSetInstance: moleculeSetInstance]
	            return
	        }

			flash.message = message(code: 'default.created.message', args: [message(code: 'moleculeSet.label', default: 'MoleculeSet'), moleculeSetInstance.id])
	        redirect action: 'show', id: moleculeSetInstance.id
			break
		}
    }

    def show() {
        def moleculeSetInstance = MoleculeSet.get(params.id)
        if (!moleculeSetInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'moleculeSet.label', default: 'MoleculeSet'), params.id])
            redirect action: 'list'
            return
        }

        [moleculeSetInstance: moleculeSetInstance]
    }

    def edit() {
		switch (request.method) {
		case 'GET':
	        def moleculeSetInstance = MoleculeSet.get(params.id)
	        if (!moleculeSetInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'moleculeSet.label', default: 'MoleculeSet'), params.id])
	            redirect action: 'list'
	            return
	        }

	        [moleculeSetInstance: moleculeSetInstance]
			break
		case 'POST':
	        def moleculeSetInstance = MoleculeSet.get(params.id)
	        if (!moleculeSetInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'moleculeSet.label', default: 'MoleculeSet'), params.id])
	            redirect action: 'list'
	            return
	        }

	        if (params.version) {
	            def version = params.version.toLong()
	            if (moleculeSetInstance.version > version) {
	                moleculeSetInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
	                          [message(code: 'moleculeSet.label', default: 'MoleculeSet')] as Object[],
	                          "Another user has updated this MoleculeSet while you were editing")
	                render view: 'edit', model: [moleculeSetInstance: moleculeSetInstance]
	                return
	            }
	        }

	        moleculeSetInstance.properties = params

	        if (!moleculeSetInstance.save(flush: true)) {
	            render view: 'edit', model: [moleculeSetInstance: moleculeSetInstance]
	            return
	        }

			flash.message = message(code: 'default.updated.message', args: [message(code: 'moleculeSet.label', default: 'MoleculeSet'), moleculeSetInstance.id])
	        redirect action: 'show', id: moleculeSetInstance.id
			break
		}
    }

    def delete() {
        def moleculeSetInstance = MoleculeSet.get(params.id)
        if (!moleculeSetInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'moleculeSet.label', default: 'MoleculeSet'), params.id])
            redirect action: 'list'
            return
        }

        try {
            moleculeSetInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'moleculeSet.label', default: 'MoleculeSet'), params.id])
            redirect action: 'list'
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'moleculeSet.label', default: 'MoleculeSet'), params.id])
            redirect action: 'show', id: params.id
        }
    }
	
	def createSampleMoleculeSet () {
		moleculeSetService.createSampleMoleculeSet()
	}
	
	def calculateCorrelations () {
		[ result : moleculeSetService.calculateCorrelations(params.id), id: params.id ]
	}
	
	def heatmap () {
		def _moleculeSet = MoleculeSet.get(params.id)
		
		/* Server side window stuff - test -
		new groovy.swing.SwingBuilder().frame(title:'v1.1',pack:true,show:true){
			panel(mousePressed: {e->
				if (e.isPopupTrigger()) {
					popupMenu {
						menuItem{
							action(name:'Exit', closure:{System.exit(0)})
						}
					}.show(e.getComponent(), e.getX(), e.getY())
				}
			}){
				label("Right click here to get a popup menu to Exit")
			}
		}*/
		
		def _return = []

		if (_moleculeSet.correlationDescriptorSet==null) return "Failed"
		
		// For each correlation descriptor set, get all data from the MMDS table		
		_moleculeSet.correlationDescriptorSet.each { // Groovy ;)
			def descriptorSet = it
			_return += MoleculeMoleculeDescriptorSet.createCriteria().list {
				createAlias("molecule1", "m1")
				createAlias("molecule2", "m2")
				and {
					eq('descriptorSet', descriptorSet ) // default
					eq('m1.moleculeSet', _moleculeSet)
					eq('m2.moleculeSet', _moleculeSet)
				}
				order('m1.id','asc')
				order('m2.id','asc')
			}
		}

		List data = moleculeMoleculeDescriptorSetService.MoleculeMoleculeDescriptorSetToD3Data(_return); // data for heatmap	
		
		[  mol_js : data[0], heatmap_js : data[1] ]
	}

}
