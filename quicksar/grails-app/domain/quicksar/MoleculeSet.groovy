package quicksar

class MoleculeSet {
	
	static transactional = true
	
	static hasMany = [
		molecule : Molecule, 
		descriptorSet : DescriptorSet, 
		correlationDescriptorSet : DescriptorSet // DescriptorSets, for which the correlation matrix has been calculated
	]
	
	String name
	
    static constraints = {
		molecule(nullable:true)
		descriptorSet(nullable:true)
		correlationDescriptorSet(nullable:true)
    }
}
