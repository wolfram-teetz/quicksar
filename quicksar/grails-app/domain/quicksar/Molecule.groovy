package quicksar

class Molecule {

	static transactional = true
	
	static belongsTo = [moleculeSet : MoleculeSet]
	
	String smiles	// Smiles representation
	String inchiKey // For duplicates checking
	
	static hasMany = [integerDescriptor : IntegerDescriptor]
	
	
    static constraints = {
		smiles()
		moleculeSet()
		inchiKey()
    }
}
