package quicksar

class DescriptorSet {

	static hasMany = [descriptor : Descriptor]
	
	String name  // Descriptor set name, e.g. QNPR
	
    static constraints = {
		descriptor()
    }
}
