package quicksar

class Descriptor {

	static belongsTo = [descriptorSet : DescriptorSet]
	String name	// Descriptor name, e.g. CCCO
				
    static constraints = {
    }
	
}
