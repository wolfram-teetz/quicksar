package quicksar

class IntegerDescriptor {

	static belongsTo = [descriptor : Descriptor, molecule : Molecule]
	
	int value; // descriptor value
	
    static constraints = {
		molecule()
		descriptor()
		value()
    }
}
