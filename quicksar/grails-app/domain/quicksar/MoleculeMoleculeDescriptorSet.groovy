package quicksar

class MoleculeMoleculeDescriptorSet {

	static belongsTo = [descriptorSet : DescriptorSet, molecule1 : Molecule, molecule2 : Molecule]

	int hit; // manhattan distance between two molecules on descriptor set x
	int miss;
		
    static constraints = {
		molecule1()				// molecule1id <= molecule2.id by definition, here??
		molecule2()
		descriptorSet()
		hit()
		miss()
    }
}
